package algorithmsproject3;

/*
 * DisjointSet class provides a template for DisjointSet objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: DisjointSet.java
 * Created: Oct 2017
 * ęCopyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

public class DisjointSet {
    private Node head;
    
    public DisjointSet() {
        this(null);
    }
    
    public DisjointSet(Node head) {
        this.head = head;
    }
    
    public Node getHead() {
        return this.head;
    }
    
    public void setHead(Node head) {
        this.head = head;
    }
}
