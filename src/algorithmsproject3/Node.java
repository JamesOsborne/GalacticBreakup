package algorithmsproject3;

/*
 * Node class provides a template for Node objects
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: Node.java
 * Created: Oct 2017
 * ęCopyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

public class Node {
    private Node parent;
    private Coord coordinate;
    private int height;
    
    public Node() {
        this(null, new Coord(), 0);
        this.parent = this;
    }
    
    public Node(Coord coord) {
        this(null, coord, 0);
        this.parent = this;
    }
    
    public Node(Node parent, Coord coord, int height) {
        this.parent = parent;
        this.height = height;
        this.coordinate = coord;
    }
    
    public Node getParent() {
        return this.parent;
    }
    
    public int getHeight() {
        return this.height;
    }
    
    public Coord getCoordinate() {
        return this.coordinate;
    }
    
    public void setParent(Node parent) {
        this.parent = parent;
    }
    
    public void setHeight(int height) {
        this.height = height;
    }
    
    public void setCoordinate(Coord coord) {
        this.coordinate = new Coord(coord);
    }
}
