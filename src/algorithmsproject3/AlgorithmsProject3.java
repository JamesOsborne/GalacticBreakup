package algorithmsproject3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*
 * The main class for the galactic breakup project 
 *
 * @author James Osborne & Nate Stahlnecker
 * @version 1.0
 * File: DisjointSet.java
 * Created: Oct 2017
 * ęCopyright Cedarville University, its Computer Science faculty, and the
 * authors. All rights reserved.
 * Summary of Modifications:
 *
 */

public class AlgorithmsProject3 {
    public static void main(String[] args) throws FileNotFoundException {
        File input = new File(args[0]);
        Scanner scan = new Scanner(input);
        
        int problems = scan.nextInt();
        
        for (int i = 0; i < problems; ++i) {
            int result = 0;
            
            int x = scan.nextInt();
            int y = scan.nextInt();
            int z = scan.nextInt();
            
            Galaxy galaxy = new Galaxy(x, y, z);
            List<int[]> months = new ArrayList<>();
            
            int lineCount = scan.nextInt();
            
            for (int j = 0; j < lineCount; ++j) {
                int dominionsToRemove = scan.nextInt();
                int[] month = new int[dominionsToRemove];
                
                for (int k = 0; k < dominionsToRemove; ++k) {
                    month[k] = scan.nextInt();
                }
                
                months.add(month);
            }
            
            for (int j = months.size() - 1; j >= 0; --j) {
                int[] month = months.get(j);
                
                for (int k = month.length - 1; k >= 0; --k) {
                    galaxy.makeSet(new Node(makeCoord(month[k], x, y)));
                }
                
                if (galaxy.getSetCount() > 1) {
                    ++result;
                }
            }
            
            System.out.println(result);
        }
    }
    
    private static Coord makeCoord(int position, int width, int height) {
        int x = position % width;
        int z = position / (width * height);
        int y = (position - (z * width * height)) / height;
        
        return new Coord(x, y, z);
    }
}
